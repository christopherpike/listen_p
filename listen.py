import sys
from Student import Student
import jinja2

#listen

list=[1,2,3,5,8,4,2]

print(list)

print(len(list))

for n in list:
    print(n)

#list=list.sort()

print("sorted, hopefully")
for n in list:
    print(n)

buchstaben=['a','b','c','e','d']

mischmasch=[1,'e']

print(buchstaben[3]+" sollte e sein")

print(buchstaben[-1]+" sollte d sein")

print(mischmasch)

print((buchstaben[2:4])) #ende ist exclusive

satz="Heute ist ein schöner Tag."

word_chain=["Heute","ist","ein","schöner","Tag","."," "]

print(word_chain)

print("Heute" in word_chain)

print("heute" in word_chain)

print("Lists are mutable. And next thing is substitution")

print(len(word_chain))

print(word_chain[3])

print(word_chain)

word_chain[3]="blöder"

print(word_chain)

word_chain[3:5]=["vader","Dag"]

print(word_chain)

print(max(word_chain))

print(min(word_chain))

buchstaben_sortiert=sorted(buchstaben)

buchstaben_flip_reverse=sorted(buchstaben, reverse=True)

print(buchstaben_sortiert)

print(buchstaben_flip_reverse)

name="Name"

print(".join, but only for strings %s"%name)

print(word_chain[-1].join(word_chain[:-1]))

print(".append(element)")

# 4
start_routine="""
def main():
    hier steht der code

if __name__=="__main__":
    main()
"""

#https://wiki.python.org/moin/BeginnersGuide

#https://empireofcode.com/game/#

temp="Hmm"
if(len(sys.argv)==2):
    print(f"Hello, my Name is {sys.argv[1]}.")
    print("Hallo, mein Name ist {}, und ihr Name ist {}.".format(sys.argv[1],temp))

print(len(sys.argv))
print(sys.argv[0])

buecher=[
    "Buch Eins",
    "Mose",
    "Telephonbuch",
    "Telefonbuch",
    "Telephonbook",
    "The Game"]

suche="Mose"

if suche in buecher:
    print(f"Hier bitte, da hast du {suche}.")
else:
    print(f"Ich habe kein {suche}.")

maik=Student("Maik","Müller","Windmühle")

print(maik)

print(maik.zusammenfassung())

maik.drucke_zusammenfassung()

print("set = no duplicates, only uniques, maybe sorted")

zahlen_set=set([1,2,4,5,5,5,3,7,8,9,0,11])

buecher.append("The Game")

buecher.append("Donald Duck")

buecher.append("Donald Duck")

wort_set=set(buecher)

print(len(zahlen_set))

print(len(wort_set))

print(zahlen_set)

print(wort_set)

wort_set=sorted(wort_set)

print(wort_set)

print("Dictionaries WITH CURLY BRACESES!!!!, Key:Value, and add with add")

dictionary_num={1:2,2:2,3:145}

dictionary_str={'apple':4,'pie':3,'banana':len('banana')}

print(type(dictionary_num))

print(dictionary_num)

print(type(dictionary_str))

print(dictionary_str)

print(len(dictionary_str))

print("How to look up dictionaries? dictionary[key] gives me the value")

print("dict_str dictionary_str['apple'] is "+str(dictionary_str['apple']))

print(sorted(dictionary_str))

#nicht sortierbar?


dictionary_mix={'Word':'Wort',3:3,'zahl':4,42:'zweiundvierzig'}

print(type(dictionary_mix))

print(dictionary_mix)

print(dictionary_mix['Word'])

print(dictionary_mix[3])

#print(dictionary_mix['word'])

print("nested things inside a dict")

nested_dictionary={"Deutsch-English":
                   {"Apfel":"apple","Auto":"car","Birne":"pear"},
                   "Deutsch-Francoise":
                   {"Apfel":"pomme","Auto":"automobile","Birne":"brine"}}

print(nested_dictionary)

print(nested_dictionary["Deutsch-Francoise"])

print("Tuples, one or more elements separated by comma")

print('tuple_single=(12566) or dortmund=("Landeshauptstadt","NRW")')

tuple_single=(12566)

tuple_double=(45,5466)

tuple_triple=(458,558.5,8)

tuple_str=("Landeshauptstadt","NRW")

dortmund=("Landeshauptstadt","NRW")

#print(str(tuple_single()))

print(dortmund)


datei=open('./temp.html','w')

datei_inhalt="""<!DOCTYPE html>
<html>
<head>
<title>{{dateiueberschrift}}</title>
</head>
<body>
<h1>Hier kommt der Inhalt</h1>
</body>
</html>"""

datei.write(datei_inhalt)

datei.close()

print("Echo")

template_index=jinja2.Template(datei_inhalt)

print(template_index.render(dateiueberschrift="Der Gerät"))

