import os
import time
import sys
from flask import Flask,request,render_template,redirect
import jinja2
import cgi

#on windows run with : set FLASK_APP=dateiname.py
# flask run ///geil d:\programme\python\python -m flask run

application=Flask(__name__)

@application.route('/')
def get_():
    warum='<!DOCTYPE html><a href="/shoppingcart">Nochmal</a>'
    return warum

@application.route('/shoppingcart',methods=["GET","POST"])
def get():
    
    if request.method=='POST':
        if not request.form['chbox']:
            request_form_chbox="null"
        else:
            request_form_chbox=request.form['chbox']
        if not request.form['radio_group']:
            request_form_radio_group="null"
        else:
            request_form_radio_group=request.form['radio_group']
        if (ueberpruefe_tag(request.form['tag']) and ueberpruefe_monat(request.form['monat']) and ueberpruefe_jahr(request.form['jahr'])):
            return redirect('/dankeschoen',code=302)
        else:
            return """<!DOCTYPE html><a href="/shoppingcart">Nochmal</a>
                    
                    <h3>q-%s</h3>
                    <h3>pass-%s</h3>
                    <h3>h-%s</h3>
                    <h3>cb-%s</h3>
                    <h3>radio_one-%s</h3>
                    <h3>Monat-%s</h3>
                    <h3>Tag-%s</h3>
                    <h3>Jahr-%s</h3>
                """%(request.form['q'],request.form['pass'],request.form['h'],request_form_chbox,request_form_radio_group,
                     ueberpruefe_monat(request.form['monat']),ueberpruefe_tag(request.form['tag']),ueberpruefe_jahr(request.form['jahr']))
            #'''         
    else:
        return zeiche_form("Fehler","Monat","01","1990")

@application.route('/dankeschoen')
def post():
    return "<h1>Danke schön</h1>"

def zeiche_form(error="",monat="Monat",tag="01",jahr="1990"):
    deshalb="""<!DOCTYPE html><form action="/shoppingcart" method="POST">
    <h3 style="color: red;">%(error)s</h3>
    <label>Text
        <input type="text" name="q"/>
    </label><label>Password
        <input type="password" name="pass"/>
    </label><label>Hidden
        <input type="hidden" name="h" value="True"/>
    </label><label>CB ON or value
        <input type="checkbox" name="chbox" checked="checked"/>
    </label><label>RB On
        <input type="radio" name="radio_group" checked="checked"/>
    </label><label>Select Eis
    <select name="eissorte">
        <option value="nuss">Erdnuss</option>
        <option >Schoki</option>
        <option value="vanillie">Vanillie</option>
        <option value="Erdbeere" selected="selected">Erdbeere</option>
    </select>
    </label><label>Monat
        <input type="text" name="monat" value="%(monat)s"/>
    </label><label>Tag
        <input type="text" name="tag" value="%(tag)s"/>
    </label><label>Jahr
        <input type="text" name="jahr" value="%(jahr)s"/>
    </label>
        <input type="submit" name="absenden"/>
    </form>"""%{'error':error,'monat':monat,'tag':tag,'jahr':jahr}
    return deshalb

def ueberpruefe_monat(monat):
    if monat and monat.isdigit():
        monat=int(monat)
        if(monat>0 and monat<=12):
            return monat

def ueberpruefe_tag(tag):
    if tag and tag.isdigit():
        tag=int(tag)
        if(tag>0 and tag<=31):
            return tag

def ueberpruefe_jahr(jahr):
    if jahr and jahr.isdigit():
        jahr=int(jahr)
        if(jahr>1900 and jahr<4000):
            return jahr

def escape_html(s):
    return cgi.escape(s, quote=True)
