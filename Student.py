class Student:
    def __init__(self,vor_name,nach_name,wg):
        self.vor_name=vor_name
        self.nach_name=nach_name
        self.wg=wg

    def zusammenfassung(self):
        return self.vor_name+" "+self.nach_name+" wohnt in der WG "+self.wg

    def drucke_zusammenfassung(self):
        print(self.vor_name+" "+self.nach_name+" wohnt in der WG "+self.wg)
