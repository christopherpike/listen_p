import cgi
from flask import Flask,request,redirect

application=Flask(__name__)

@application.route('/')
def get():
    return '<label>ROT13<form action="/rot" method="POST"><input type="textarea" name="textinput"/></form>'

@application.route('/rot',methods=["GET","POST"])
def post():
    if request.form['textinput']:
    #and request.form['textinput'].isstr():
        return rot13(request.form['textinput'])

def rot13(text):
    text_return=""
    small_abc={
        'a':'n',
        'b':'o',
        'c':'p',
        'd':'q',
        'e':'r',
        'f':'s',
        'g':'t',
        'h':'u',
        'i':'v',
        'j':'w',
        'k':'x',
        'l':'y',
        'm':'z',
        'n':'a',
        'o':'b',
        'p':'c',
        'q':'d',
        'r':'e',
        's':'f',
        't':'g',
        'u':'h',
        'v':'i',
        'w':'j',
        'x':'k',
        'y':'l',
        'z':'m'
        }
    dots={'!':'&1;',
        '&':'&amp;',
        '<':'&lt;',
        '>':'&gt;',
        '"':'&qoute;'}

    if text:
        for s in text:
            if s.islower():
                text_return+=small_abc.get(s,s)
            elif s.isupper():
                text_return+=small_abc.get(s.lower(),s).upper()
            elif s.isdecimal():
                text_return+=s
            elif s.isspace:
                text_return+=s
            elif s.isprintable():
                if s in dots:
                   text_return+=dots.get(s,s) 
        return text_return
